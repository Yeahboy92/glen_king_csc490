Shader "Custom/AlphaBlended" 
{
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	
	Category 
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent"}
		Cull Off Lighting Off ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
	
		BindChannels
		{
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}
		
		SubShader 
		{
			Pass
			{
				SetTexture{_MainTex}
				{
					combine texture * primary
				}
			}
		}
	} 
	
	FallBack "Diffuse"
}
