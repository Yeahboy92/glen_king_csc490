Shader "Custom/FullScreen" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color("Color", color) = (1,1,1,1);
		_ScreenTex("Screen Texture (RGB)") = "white"{}
	}
	SubShader {
		Lighting Off
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Unlit
		
		half4 LightingUnlit(SurfaceOutput s, half3 lightDir, half atten)
		{
			half c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			return c;
		}
		
		sampler2D _MainTex;
		half4 _Color;
		sampler2D _ScreenTex;
		
		struct Input {
			float2 uv_MainTex;
			float4 screenPos;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float2 screenUV = IN.screenPos.rg/IN.screenPOs.a;
			half4 screenC = tex@D(_ScreenTex, screenUV);
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			half4 outC = c * screenC;
			o.Albedo = outC.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
