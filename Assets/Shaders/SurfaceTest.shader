Shader "Custom/SurfaceTest" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Color to blend to", color) = (1,1,1,1)
		_Blend ("Blend", Range(0,1)) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;
		float _Blend;
		half4 _Color;
		
		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = lerp(tex2D(_MainTex, IN.uv_MainTex), _Color, _Blend);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
