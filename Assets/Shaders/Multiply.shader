Shader "Custom/Multiply" {
	
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent"}
		Cull Off Lighting Off ZWrite Off
		Blend Zero SrcColor
		
	
		BindChannels
		{
			Bind "Color", color
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
		}
		
		Pass
		{
			SetTexture{_MainTex}
			{
				combine texture * primary
			}
			
			SetTexture{_MainTex}
			{
				constantColor(1, 1, 1, 1)
				combine previous lerp(previous)
			}
		}
		
	} 
	
	FallBack "Diffuse"
}