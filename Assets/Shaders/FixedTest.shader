Shader "Custom/FixedTest" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags("Queu"="Transparent" "RenderType"="Transparent")
		Blend One One
		BlendOp Min
		Cull Off Lighting Off ZWrite Off
		
		BindChannels
		{
			Bind "Color"
			Bind ""
			Bind ""
			Bind ""
		}
		
		Pass
		{
			Material
			{
				Diffuse
			}
			SetTexture[_MainTex]
			{
				combine texture * primary
			}
		}
		
	} 
	FallBack "Diffuse"
}
