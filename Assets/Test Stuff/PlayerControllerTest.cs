using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MotorTest))]

public class PlayerControllerTest : MonoBehaviour {

	public float speed = 1.0f;
	MotorTest motor;
	
	
	void Start () 
	{
		motor = GetComponent<MotorTest>();
	}
	
	
	void Update () 
	{
		motor.targetVelocity.x = Input.GetAxis("Horizontal") * speed;
		motor.shouldJump = Input.GetButtonDown("Jump");
		
	}
}
