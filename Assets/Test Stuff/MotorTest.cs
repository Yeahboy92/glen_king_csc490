using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]

public class MotorTest : MonoBehaviour {
public float speed = 1.0f;

	int doubleJump = 0;
	public float maxSpeedChange = 1.0f;
	public float jumpHeight = 1.0f;
	
	bool grounded = true;
	
	
    [HideInInspector]
	public bool shouldJump = false;
	
	[HideInInspector]
	public Vector3 targetVelocity = Vector3.forward;
	
	[HideInInspector]
	public int life = 3;
	
	
	Vector3 contactNormGround = Vector3.zero;
	
	
	public bool OnGround()
	{
		return grounded;
	}
	
	public int Life()
	{
		return life;
	}
	
//	public bool InAir()
//	{
//		return !grounded;
//	}
	
	
	void FixedUpdate () 
	{
		
		Vector3 velocity = rigidbody.velocity;
		Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange = velocityChange.normalized * Mathf.Clamp(velocityChange.magnitude, 0, maxSpeedChange);
		velocityChange.y = 0;
		rigidbody.AddForce((targetVelocity * speed), ForceMode.VelocityChange);
	
		if(shouldJump && grounded)
		{
			rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
			grounded = true;
			doubleJump = 0;
		}
		
		if(shouldJump && doubleJump < 2)
		{
			rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
			grounded = false;
			doubleJump++;
		}

		
	}
	
	void OnTriggerStay()
	{
		if(GameObject.Find("ladder"))
			rigidbody.velocity = Vector3.up * 2;
	}
	
	
	void OnCollisionStay(Collision collision)
	{
			
		foreach(ContactPoint cp in collision.contacts)
		{
			contactNormGround = cp.normal;
			
			
		//  float groundNorm = Mathf.Rad2Deg * Mathf.Atan2 (contactNormGround.y, Mathf.Abs(contactNormGround.x));
		//	float wallNorm = Mathf.Rad2Deg * Mathf.Atan2 (contactNormWallLeft.y, Mathf.Abs(contactNormWallLeft.x));
			
			if (contactNormGround.y > Mathf.Abs(contactNormGround.x))
				grounded = true;
			
			else if(contactNormGround.y < Mathf.Abs(contactNormGround.x))
				grounded = false;
			
		}
	}

	
	float CalculateJumpVerticalSpeed()
	{
		float g = Physics.gravity.y;
		return Mathf.Sqrt(2 * jumpHeight * -g);
	}
	
	
}
