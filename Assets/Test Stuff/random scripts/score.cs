using UnityEngine;
using System.Collections;

public class score : MonoBehaviour {
	
	public TextMesh text;
	
	void OnEnable()
	{
		health.onHit += OnDamage;
	}
	
	void OnDisable()
	{
		health.onHit -= OnDamage;
	}
	
	//public delegate void OnTakeDamage(int lifeLeft);
	void OnDamage(int healthLeft)
	{
		text.text = healthLeft.ToString();
	}
}
