using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Camera))]

public class Mask : MonoBehaviour {

	private RenderTexture renderTex;
	
	// Update is called once per frame
	void Update () 
	{
		if(renderTex == null || renderTex.width != Screen.width || renderTex.height != Screen.height)
		{
			DestroyImmediate(renderTex);
			renderTex = new RenderTexture(Screen.width, Screen.height, 0);
			camera.targetTexture = renderTex;
		}
	
	}
}
