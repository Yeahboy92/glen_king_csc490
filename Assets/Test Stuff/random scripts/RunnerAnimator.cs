// Original code by Joachim Ante - Extended by TomLong74
// I modified for my purposes.


using UnityEngine;
using System.Collections;

public class RunnerAnimator : MonoBehaviour 
{
	
	//vars for the whole sheet
	public int colCount = 8;
	public int rowCount = 8;
 
	//vars for animation
	public int rowNumber = 0;
	public int colNumber = 0;
	public int totalCells = 12;
	public int fps = 15;
	
  	
    private Vector2 offset;
	
	
	void Update () 
	{ 
		SetSpriteAnimation(colCount , rowCount, rowNumber, colNumber, totalCells, fps );
		
//		if(totalCells < 6)
//		SetSpriteAnimation(colCount , rowCount, rowNumber, colNumber, totalCells, fps );
//		
//		totalCells++;
//		
//		if(totalCells >= 7)
//		{
//			rowNumber = 1;
//			SetSpriteAnimation(colCount , rowCount, rowNumber, colNumber, totalCells, fps );
//		}
	}
 
	//SetSpriteAnimation
	void SetSpriteAnimation(int colCount ,int rowCount ,int rowNumber ,int colNumber,int totalCells,int fps )
	{
 
    // Calculate index
    int index  = (int)(Time.time * fps);
    
		// Repeat when exhausting all cells
    index = index % totalCells;
 
    // Size of every cell
    float sizeX = 1.0f / colCount;
    float sizeY = 1.0f / rowCount;
    Vector2 size =  new Vector2(sizeX,sizeY);
 
    // split into horizontal and vertical index
    var uIndex = index % colCount;
    var vIndex = index / colCount;
 
    // build offset
    // v coordinate is the bottom of the image in opengl so we need to invert.
    float offsetX = (uIndex+colNumber) * size.x;
    float offsetY = (1.0f - size.y) - (vIndex + rowNumber) * size.y;
    Vector2 offset = new Vector2(offsetX,offsetY);
 
    renderer.material.SetTextureOffset ("_MainTex", offset);
    renderer.material.SetTextureScale  ("_MainTex", size);
	}
	
	void OnCollisionEnter()
	{
		Destroy(gameObject);
	}
}