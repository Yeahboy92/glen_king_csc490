using UnityEngine;
using System.Collections;

public class health : MonoBehaviour 
{
	
	public delegate void OnTakeDamage(int lifeLeft);
	
	public static event OnTakeDamage onHit = delegate{};
	
	int life = 10;
	
	void OnCollisionEnter(Collision collision)
	{
		life -= 1;
		onHit(life);
	}

}
