using UnityEngine;
using System.Collections;

public class movePaddle : MonoBehaviour 
{

	public float speed = 20;

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position += Vector3.right * Input.GetAxis("Horizontal") * Time.deltaTime * speed;
		
		Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
		Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		
		if(topRight.x < 0)
		{
			transform.position += Vector3.right;
		}
		else if(bottomLeft.x > 1)
		{
			transform.position += Vector3.left;
		}
	}
	
}

