using UnityEngine;
using System.Collections;

public class playerBall : MonoBehaviour 
{
   
	[HideInInspector]
	public float minspeed = 25;
	
	[HideInInspector]
	public bool inUse = false;
	
	[HideInInspector]
	public int highscore = 0;
	
	[HideInInspector]
	public int brickCount = 26;
	
	private int life = 3;
	private int score = 0;
	
	Vector3 v;
	
	int count = 15;
	//int brickCount = 26;
	
	float followX = 0;
	float followY = 0;
	float followZ = 0;
	
	[HideInInspector]
	public TextMesh lifeCountTextMesh;
	
	[HideInInspector]
	public TextMesh scoreCountTextMesh;
	
	[HideInInspector]
	public TextMesh highScoreCountTextMesh;
	
	
	// Use this for initialization
	void Start () 
	{	
		highScoreCountTextMesh.text = PlayerPrefs.GetInt("highscore").ToString();
	}
	
	// Update is called once per frame
	void Update () 
	{

		//sets bounds
		v = rigidbody.velocity;	
		Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
		
		//makes ball follow paddle
		if(!Input.GetButton("Jump") && inUse == false)
		{
			
			followX = GameObject.Find("paddle").transform.position.x;
			followY = GameObject.Find("paddle").transform.position.y;
			followZ = GameObject.Find("paddle").transform.position.z;
		
			rigidbody.transform.position = new Vector3(followX, (followY + 2f) , followZ);
		}
		
		//checks that the space bar has been hit
		if(Input.GetButton("Jump"))
			inUse = true;
		
		//starts the game
	    if(inUse)
		{
			inUse = true;
			
			//stops the upward force
			if(count > 10)
				rigidbody.AddForce(Vector3.up * minspeed);
			
			//keeps the speed constant
			if (v.magnitude < minspeed || v.magnitude > minspeed)
			{
				v.Normalize();
				v *= minspeed;
				rigidbody.velocity = v;
			}
		
			//changes the direction of the ball when it hit the edges of the screen
			if (topRight.x > 1) 
				rigidbody.velocity = new Vector3(-Mathf.Abs(v.x), v.y, v.z);
		
			else if (bottomLeft.x < 0)
				rigidbody.velocity = new Vector3(Mathf.Abs(v.x), v.y, v.z);			
		
		
			if (topRight.y > 1) 
				rigidbody.velocity = new Vector3(v.x, -Mathf.Abs(v.y), v.z);
		
			//resets the balls position on a loss and sets remaining lives
			else if (bottomLeft.y < 0)
			{
				followX = GameObject.Find("paddle").transform.position.x;
				followY = GameObject.Find("paddle").transform.position.y;
				followZ = GameObject.Find("paddle").transform.position.z;
				
				rigidbody.transform.position = new Vector3(followX, (followY + 2f) , followZ);
				
				life--;
				lifeCountTextMesh.text = life.ToString();
				
				inUse = false;
			}
			
			count--;
			
			//restarts the game on no lives left
			if(life == 0)
			{
				this.SaveHighScore();
				Application.LoadLevel(0);
				
			}
				
			if(brickCount == 0)
			{
    			this.SaveHighScore();
				Application.LoadLevel(0);
			}
					
		}	
	}
	
	
	void OnCollisionEnter(Collision collision)
	{
		
		if(collision.gameObject.name == "paddle")
		{
		    float ballDist = 0;
		    
			if(v.x < 0)
			{
				ballDist = this.transform.position.x - GameObject.Find("paddle").transform.position.x;
				v.x += ballDist * 100;				
				this.rigidbody.AddForce(new Vector3(v.x , v.y, v.z));
			}
			
			else if(v.x > 0)
			{
				ballDist = GameObject.Find("paddle").transform.position.x - this.transform.position.x;
				v.x += ballDist * 100;	
				this.rigidbody.AddForce(new Vector3(v.x , v.y, v.z));
			}	
		}
		
		
		if(GameObject.Find("paddle"))
		{
			score++;
			scoreCountTextMesh.text = score.ToString();
		}
		
		if(collision.gameObject.name == "brickSimple")
			brickCount--;
		 
    }
	
	
	void SaveHighScore()
	{	
		if(score > PlayerPrefs.GetInt("highscore"))
		{
			highscore = score;
			PlayerPrefs.SetInt("highscore", highscore);
		}
		highScoreCountTextMesh.text = highscore.ToString();
	}
}
