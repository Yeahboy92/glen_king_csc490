using UnityEngine;
using System.Collections;

public class SaveScript : MonoBehaviour 
{
	
	[HideInInspector]
	public int highscore = 0;
	
	[HideInInspector]
	public int score = 0;
	
	//[HideInInspector]
	public TextMesh scoreCountTextMesh;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	void OnCollisionEnter(Collision collision)
	{
		if(collision.gameObject.name == "EndOfLevel")
		{
			this.SaveHighScore();
			Application.LoadLevel("Scene3_EndScene");
		}
			
		if(collision.gameObject.name == "stationaryGerm")
		{
			score++;
			scoreCountTextMesh.text = score.ToString();
		}
	}
	
	void SaveHighScore()
	{	
		if(score > PlayerPrefs.GetInt("highscore"))
		{
			highscore = score;
			PlayerPrefs.SetInt("highscore", highscore);
		}
	}
	
	public int GetHighScore()
	{
		return highscore;
	}
}
