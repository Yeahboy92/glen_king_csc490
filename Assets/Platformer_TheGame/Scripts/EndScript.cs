using UnityEngine;
using System.Collections;


public class EndScript : MonoBehaviour 
{
	
	[HideInInspector]
    public TextMesh highscoreTextMesh;
	
	[HideInInspector]
	public int highscore;
	
	SaveScript save;
	
	void Start()
	{
		save = GetComponent<SaveScript>();
		
		highscore = save.GetHighScore();		
	}
	
	void Update() 
	{
		highscoreTextMesh.text = highscore.ToString();
		
		if (Input.GetKeyDown(KeyCode.Space))
			Application.LoadLevel("Scene2_GameOn"); 
		
    }
	
   
       
}
