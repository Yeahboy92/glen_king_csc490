using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Motor))]

public class PlayerController : MonoBehaviour {
	
	public float speed = 1.0f;
	Motor motor;
	
	// Use this for initialization
	void Start () 
	{
		motor = GetComponent<Motor>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		motor.targetVelocity.x = Input.GetAxis("Horizontal") * speed;
		motor.shouldJump = Input.GetButton("Jump");
		
		if(motor.Life() == 2)
			Destroy(GameObject.Find("life03"));
		
		if(motor.Life() == 1)
			Destroy(GameObject.Find("life02"));
		
		if(motor.Life() == 0)
		{
			Destroy(GameObject.Find("life01"));
			Application.LoadLevel("Scene2_GameOn");
		}
	}
}
