using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]


public class Motor : MonoBehaviour 
{
	public float speed = 1.0f;
	public float maxSpeedChange = 1.0f;
	public float jumpHeight = 1.0f;
	
	bool grounded = false;
	bool grabingWall = false;
	bool grabingWallLeft = false;
	bool grabingWallRight = false;
	
	
	
    [HideInInspector]
	public bool shouldJump = false;
	
	[HideInInspector]
	public Vector3 targetVelocity = Vector3.forward;
	
	[HideInInspector]
	public int life = 3;
	
	
	Vector3 contactNormGround = Vector3.zero;
	Vector3 contactNormWallLeft = Vector3.zero;
	Vector3 contactNormWallRight = Vector3.zero;
	
	
	void Start () 
	{
			
	}
	
	public bool OnGround()
	{
		return grounded;
	}
	
	
	public bool GrabingWallLeft()
	{
		return grabingWallLeft;
	}
	
	
	public bool GrabingWallRight()
	{
		return grabingWallRight;
	}
	
	
    public bool GrabingWall()
	{
		return grabingWall;	
	}
	
	
	public int Life()
	{
		return life;
	}
	
//	public bool InAir()
//	{
//		return !grounded && !grabingWall && !grabingWallLeft && !grabingWallRight;
//	}
	
	// Update is called once per frame
	
	void FixedUpdate () 
	{
		Vector3 velocity = rigidbody.velocity;
		Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange = velocityChange.normalized * Mathf.Clamp(velocityChange.magnitude, 0, maxSpeedChange);
		velocityChange.y = 0;
		rigidbody.AddForce(targetVelocity, ForceMode.VelocityChange);
	
		if ((shouldJump && grounded) || (shouldJump && grabingWall))
		{
			rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
			grounded = false;
			grabingWall = false;
			grabingWallLeft = false;
			grabingWallRight = false;
		}
		
	}
	
	void OnTriggerEnter()
	{
		if(GameObject.Find("virus"))
			life--;
	}
	
	void OnCollisionStay(Collision collision)
	{
			
		foreach(ContactPoint cp in collision.contacts)
		{
			contactNormGround = cp.normal;
			contactNormWallLeft = cp.normal;
			contactNormWallRight = cp.normal;
			
		//  float groundNorm = Mathf.Rad2Deg * Mathf.Atan2 (contactNormGround.y, Mathf.Abs(contactNormGround.x));
		//	float wallNorm = Mathf.Rad2Deg * Mathf.Atan2 (contactNormWallLeft.y, Mathf.Abs(contactNormWallLeft.x));
			
			if (contactNormGround.y > Mathf.Abs(contactNormGround.x))
				grounded = true;
			
			else if(contactNormGround.y < Mathf.Abs(contactNormGround.x))
				grounded = false;
			
			if (contactNormWallLeft.x > Mathf.Abs(contactNormWallLeft.y))
			{
				grabingWall = true;
				grabingWallRight = true;
			}
			
			else if(contactNormWallLeft.x < Mathf.Abs(contactNormWallLeft.y))
				grabingWall = false;
			
			if (Mathf.Abs(contactNormWallRight.x) > Mathf.Abs(contactNormWallRight.y))
			{
				grabingWall = true;
				grabingWallLeft = true;
			}
			
			else if(contactNormWallRight.x < Mathf.Abs(contactNormWallRight.y))
				grabingWall = false;
		}
	}
	

	
	float CalculateJumpVerticalSpeed()
	{
		float g = Physics.gravity.y;
		return Mathf.Sqrt(2 * jumpHeight * -g);
	}
	
	
}
